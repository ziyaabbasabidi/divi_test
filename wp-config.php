<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'divi_test');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'sDd9[{yWqJtff[>uRd[Qs{zFw(okbhl#|/cVKaB(6Os-#qdO.wi.R[DN)j9*.`e_');
define('SECURE_AUTH_KEY',  'JzLX/$^i?wntRp}GNT?CYXNu1^+EJE{0-;ahS/x9HKy:{tj2(sproF,.J23#taG6');
define('LOGGED_IN_KEY',    '{_lKeCllxAWXF=BdF*vb oa%HkhM-pW6w!r<nD*0,#{3!kcNy#/E/<LGXK&qf?Pr');
define('NONCE_KEY',        'kt9g|0VUVA!3bdZLQFe)hqaT]GSos`AQ9rCo}eqlJ{irjB8C?Gl4Q=Vty_G>>C>5');
define('AUTH_SALT',        'efW82Z5}wz+p{j.I{z*[Gkxo|*(d[QkT?XmZ_BFOf%Isv&B.u 4v_<B@DL!mec3E');
define('SECURE_AUTH_SALT', '{xy x(Dm~+5(o`)<PM~=xN5+kd}uJeZp>I/hA-+?oIeW&qY5+@Rep.;x{7dpx^Bd');
define('LOGGED_IN_SALT',   'kn_fr+|MK3.qgIQ&hB/QRy]<MrroS4aHHSyMTZM&47mGq=`ROJ6/B|D6=HUkLt, ');
define('NONCE_SALT',       'VQ|-;?xV/f$2}I>S-,P~9isK{TKF*AUqk|y0DU)#k<rEre:;W!$ZBwQaba%>wxjW');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
